<?php

class API
{
    //private $serviceURL = 'http://ela.talentaim.com/api/';
    private $serviceURL = 'http://panel.elawoman.com/api/';
    private $appKey = '7b5eea7fc41322da78bc6791224fc6b0';
    private $apiVer = '2';
    private $via = 'web';
    private $requestType;
    private $requestData = '{}';
    private $authToken;
    private $memberRef;

    function __construct()
    {
        $this->authToken = isset($_SESSION['authToken'])? $_SESSION['authToken'] : "";
        $this->memberRef = isset($_SESSION['memberRef'])? $_SESSION['memberRef'] : "";
    }

    public function getCurlValue($filename, $contentType, $postname)
    {
        // PHP 5.5 introduced a CurlFile object that deprecates the old @filename syntax
        // See: https://wiki.php.net/rfc/curl-file-upload
        if (function_exists('curl_file_create')) {
            return curl_file_create($filename, $contentType, $postname);
        }

        // Use the old style if using an older version of PHP
        $value = "@".realpath($filename).";filename=" . $postname;
        if ($contentType) {
            $value .= ';type=' . $contentType;
        }

        return $value;
    }

    public function sendRequest()
    {
        $form_url = $this->serviceURL.$this->requestType;
        $data_to_post = array();
        $data_to_post['appKey'] = $this->appKey;
        $data_to_post['apiVer'] = $this->apiVer;
        $data_to_post['via'] = $this->via;
        $data_to_post['requestData'] = $this->requestData;
        $data_to_post['requestType'] = $this->requestType;
        $data_to_post['authToken']   = $this->authToken;
        $data_to_post['memberRef'] = $this->memberRef;
        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL, $form_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl,CURLOPT_POST, sizeof($data_to_post));
        curl_setopt($curl,CURLOPT_POSTFIELDS, $data_to_post);
        $result = curl_exec($curl);
        curl_close($curl);
        $decoded = json_decode($result);
        if (isset($decoded->response->status) && $decoded->response->status == 'ERROR')
        {
            die('error occurred: ' . $decoded->response->errormessage);
        }
        return $decoded;
    }

    public function send_service_request($name, $email, $country_code, $mobile, $service_str)
    {
        $this->requestType = 'tikloan_request_without_member';
        $this->requestData = json_encode(array(
          'service_str'=>$service_str,
          'country_code'=>$country_code,
          'mobile'=>$mobile,
          'name'=>$name,
          'email'=>$email
        ));
        $response = $this->sendRequest();
        return $response;
    }
    public function send_service_request_emi($name, $email, $country_code, $mobile, $service_str,$comment)
    {
        $this->requestType = 'tikloan_request_without_member';
        $this->requestData = json_encode(array(
          'service_str'=>$service_str,
          'country_code'=>$country_code,
          'mobile'=>$mobile,
          'name'=>$name,
          'email'=>$email,
          'comment'=>$comment
        ));
        $response = $this->sendRequest();
        return $response;
    }
}
?>
